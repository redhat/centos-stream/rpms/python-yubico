#!/usr/bin/python3
"""Simple test for APIs used by IPA's otptoken plugin
"""
import logging

import yubico
import usb.core

logging.basicConfig(level=logging.INFO)
log = logging.getLogger()


def main():
    try:
        yk = yubico.find_yubikey()
    except usb.core.USBError as e:
        log.info(e)
    except yubico.yubikey.YubiKeyError as e:
        log.info(e)
    else:
        assert yk.version_num()
        log.info(yk.status())
        log.info(yk.status().valid_configs())
    log.info("PASS")


if __name__ == "__main__":
    main()
